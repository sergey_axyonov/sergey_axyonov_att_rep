﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttentionSystem
{
    class Matrix
    {
        public static int MaximumOfMatrix(int[][] matrix, int height, int width)
        {
            int maxValue = matrix[0][0];
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    if (matrix[i][j] > maxValue)
                        maxValue = matrix[i][j];
                }
            return maxValue;
        }
        public static double MaximumOfMatrix(double[][] matrix, int height, int width)
        {
            double maxValue = matrix[0][0];
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    if (matrix[i][j] > maxValue)
                        maxValue = matrix[i][j];
                }
            return maxValue;
        }
        public static double MinOfMatrix(double[][] matrix, int height, int width)
        {
            double minValue = matrix[0][0];
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    if (matrix[i][j] < minValue)
                        minValue = matrix[i][j];
                }
            return minValue;
        }
        public static int MinOfMatrix(int[][] matrix, int height, int width)
        {
            int minValue = matrix[0][0];
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    if (matrix[i][j] < minValue)
                        minValue = matrix[i][j];
                }
            return minValue;
        }
        public static int[][] MatrixScaling(int[][] matrix, int height, int width, double factor)
        {
            int[][] scaledMatrix = new int[height][];
            for (int i = 0; i < height; i++)
                scaledMatrix[i] = new int[width];
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                    scaledMatrix[i][j] = (int)Math.Round(matrix[i][j] * factor);
            return scaledMatrix;
        }
        public static int[][] MatrixScaling(double[][] matrix, int height, int width, double factor)
        {
            int[][] scaledMatrix = new int[height][];
            for (int i = 0; i < height; i++)
                scaledMatrix[i] = new int[width];
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                    scaledMatrix[i][j] = (int)Math.Round(matrix[i][j] * factor);
            return scaledMatrix;
        }
        public static int[][] MatrixSubstruction(int[][] aMatrix, int[][] bMatrix,
           int matrixHeight, int matrixWidth)
        {
            int[][] substructionResult = new int[matrixHeight][];
            for (int i = 0; i < matrixHeight; i++)
                substructionResult[i] = new int[matrixWidth];

            for (int i = 0; i < matrixHeight; i++)
                for (int j = 0; j < matrixWidth; j++)
                    substructionResult[i][j] = aMatrix[i][j] - bMatrix[i][j];
            return substructionResult;
        }

        public static double[][] MatrixSubstruction(double[][] aMatrix, double[][] bMatrix,
          int matrixHeight, int matrixWidth)
        {
            double[][] substructionResult = new double[matrixHeight][];
            for (int i = 0; i < matrixHeight; i++)
                substructionResult[i] = new double[matrixWidth];

            for (int i = 0; i < matrixHeight; i++)
                for (int j = 0; j < matrixWidth; j++)
                    substructionResult[i][j] = aMatrix[i][j] - bMatrix[i][j];
            return substructionResult;
        }

        public static int[][] MatrixAbsolute(int[][] matrix, int matrixHeight, int matrixWidth)
        {
            int[][] absMatrix = new int[matrixHeight][];
            for (int i = 0; i < matrixHeight; i++)
                absMatrix[i] = new int[matrixWidth];
            for (int i = 0; i < matrixHeight; i++)
                for (int j = 0; j < matrixWidth; j++)
                    absMatrix[i][j] = Math.Abs(matrix[i][j]);
            return absMatrix;
        }
        public static double[][] MatrixAbsolute(double[][] matrix, int matrixHeight, int matrixWidth)
        {
            double[][] absMatrix = new double[matrixHeight][];
            for (int i = 0; i < matrixHeight; i++)
                absMatrix[i] = new double[matrixWidth];
            for (int i = 0; i < matrixHeight; i++)
                for (int j = 0; j < matrixWidth; j++)
                    absMatrix[i][j] = Math.Abs(matrix[i][j]);
            return absMatrix;
        }
        public static int[][] MatrixSum(int[][] aMatrix, int[][] bMatrix,
            int matrixHeight, int matrixWidth)
        {
            int[][] sumResult = new int[matrixHeight][];
            for (int i = 0; i < matrixHeight; i++)
                sumResult[i] = new int[matrixWidth];
            for (int i = 0; i < matrixHeight; i++)
                for (int j = 0; j < matrixWidth; j++)
                    sumResult[i][j] = aMatrix[i][j] + bMatrix[i][j];
            return sumResult;
        }
        public static double[][] MatrixSum(ref double[][] aMatrix, ref double[][] bMatrix,
            int matrixHeight, int matrixWidth)
        {
            double[][] sumResult = new double[matrixHeight][];
            for (int i = 0; i < matrixHeight; i++)
                sumResult[i] = new double[matrixWidth];
            for (int i = 0; i < matrixHeight; i++)
                for (int j = 0; j < matrixWidth; j++)
                    sumResult[i][j] = aMatrix[i][j] + bMatrix[i][j];
            return sumResult;
        }

        public static double[][] MatrixSum3(ref double[][] aMatrix, ref double[][] bMatrix, ref double[][] cMatrix,
            int matrixHeight, int matrixWidth)
        {
            double[][] sumResult = new double[matrixHeight][];
            for (int i = 0; i < matrixHeight; i++)
                sumResult[i] = new double[matrixWidth];
            for (int i = 0; i < matrixHeight; i++)
                for (int j = 0; j < matrixWidth; j++)
                    sumResult[i][j] = aMatrix[i][j] + bMatrix[i][j] + cMatrix[i][j];
            return sumResult;
        }

        public static double[][] MatrixSum4(ref double[][] aMatrix, ref double[][] bMatrix, ref double[][] cMatrix, 
            ref double[][] dMatrix, int matrixHeight, int matrixWidth)
        {
            double[][] sumResult = new double[matrixHeight][];
            for (int i = 0; i < matrixHeight; i++)
                sumResult[i] = new double[matrixWidth];
            for (int i = 0; i < matrixHeight; i++)
                for (int j = 0; j < matrixWidth; j++)
                    sumResult[i][j] = aMatrix[i][j] + bMatrix[i][j]+cMatrix[i][j]+dMatrix[i][j];
            return sumResult;
        }
        public static int[][] MatrixStretching(int[][] matrix, int matrixHeight,
          int matrixWidth, int stretchingFactor)
        {
            int newHeight = matrixHeight * stretchingFactor;
            int newWidth = matrixWidth * stretchingFactor;
            int[][] stretchedMatrix = new int[newHeight][];
            for (int i = 0; i < newHeight; i++)
                stretchedMatrix[i] = new int[newWidth];
            for (int i = 0; i < matrixHeight; i++)
                for (int j = 0; j < matrixWidth; j++)
                {
                    for (int fi = 0; fi < stretchingFactor; fi++)
                        for (int fj = 0; fj < stretchingFactor; fj++)
                        {
                            stretchedMatrix[i * stretchingFactor + fi][j * stretchingFactor + fj] =
                                matrix[i][j];
                        }
                }
            return stretchedMatrix;
        }
        public static double[][] MatrixStretching(double[][] matrix, int matrixHeight,
            int matrixWidth, int stretchingFactor)
        {
            int newHeight = matrixHeight * stretchingFactor;
            int newWidth = matrixWidth * stretchingFactor;
            double[][] stretchedMatrix = new double[newHeight][];
            for (int i = 0; i < newHeight; i++)
                stretchedMatrix[i] = new double[newWidth];
            for (int i = 0; i < matrixHeight; i++)
                for (int j = 0; j < matrixWidth; j++)
                {
                    for (int fi = 0; fi < stretchingFactor; fi++)
                        for (int fj = 0; fj < stretchingFactor; fj++)
                        {
                            stretchedMatrix[i * stretchingFactor + fi][j * stretchingFactor + fj] =
                                matrix[i][j];
                        }
                }
            return stretchedMatrix;
        }
    }
}
