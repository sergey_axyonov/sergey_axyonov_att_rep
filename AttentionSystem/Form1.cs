﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttentionSystem
{
    public partial class Form1 : Form  
    {
        int[][] colorData;

        int[][] redComponents;
        int[][] greenComponents;
        int[][] blueComponents;
        int[][] yellowComponents;
        int numOfLevels = 9;

        int[][] intensityMatrix;

        int[][][] redMaps;
        int[][][] greenMaps;
        int[][][] blueMaps;
        int[][][] yellowMaps;

        int[][][] intensityMaps;

        double[][][] floatIntensityMaps;
        double[][][][] LOMaps; 

        int[] levelHeights;
        int[] levelWidths;


        int[] featureMapHeights;
        int[] featureMapWidths;

        int saliencyMapHeight;
        int saliencyMapWidth;
        double[][] saliencyMap;

        int[][][] intensityFeatureMaps;

        double[][][][] LOFeatureMaps;

        int[][][] RGFeatureMaps;
        int[][][] BYFeatureMaps;

        //double[][][] scaledPyramid;
        double[][] intensityConspiciencyMap;
        double[][] RGConspiciencyMap;
        double[][] BYConspiciencyMap;
        double[][][] LOConspiciencyMap;


        //int[][] RGConspiciencyMap;

        bool displayCheck;

        double[][][] filters;
        
        int numOfFilters = 4;
        int filterSize = 7;

        public Form1()
        {
            InitializeComponent();
        }
        
        public int[][][] GaussPyramidCreation2(int matrixHeight, int matrixWidth,
          int numOfLevels, ref int[] heights, ref int[] widths, ref int[] expandedHeights,
          ref int[] expandedWidths, ref int[][][] expandedPyramid)
        {
            int[][][] pyramid = new int[numOfLevels][][];

            expandedPyramid = new int[numOfLevels][][];
            heights = new int[numOfLevels];
            widths = new int[numOfLevels];
            expandedHeights = new int[numOfLevels];
            expandedWidths = new int[numOfLevels];

            heights[0] = matrixHeight;
            widths[0] = matrixWidth;
            expandedHeights[0] = heights[0] + 2;
            expandedWidths[0] = widths[0] + 2;

            for (int i = 1; i < numOfLevels; i++)
            {
                //listBox1.Items.Add("Level # "+i+" :");
                int factor = (int)Math.Pow(2, i);
                heights[i] = matrixHeight / factor;
                widths[i] = matrixWidth / factor;
                expandedHeights[i] = heights[i] + 2;
                expandedWidths[i] = widths[i] + 2;
            }

            for (int i = 0; i < numOfLevels; i++)
            {
                pyramid[i] = new int[heights[i]][];
                expandedPyramid[i] = new int[expandedHeights[i]][];
                for (int j = 0; j < heights[i]; j++)
                    pyramid[i][j] = new int[widths[i]];
                for (int j = 0; j < expandedHeights[i]; j++)
                    expandedPyramid[i][j] = new int[expandedWidths[i]];
            }

            return pyramid;
        }
        
        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadImage();
        }
        private double[][][] getNormPyramid(ref int[][][] pyramid, ref int[] heights, ref int[] widths,
            int numOfLevels)
        {
            double[][][] normPyramid = new double[numOfLevels][][];
            for (int k=0; k<numOfLevels; k++)
                normPyramid[k] = new double[heights[k]][];
            for (int k=0; k<numOfLevels; k++)
                for (int i=0; i<heights[k]; i++)
                    normPyramid[k][i] = new double[widths[k]];
            int globalMax = pyramid[0][0][0];
            int globalMin = pyramid[0][0][0];
            for (int k = 0; k < numOfLevels; k++)
                for (int i = 0; i < heights[k]; i++)
                    for (int j = 0; j < widths[k]; j++)
                    {
                        if (globalMin>pyramid[k][i][j])
                            globalMin = pyramid[k][i][j];
                        if (globalMax<pyramid[k][i][j])
                            globalMax = pyramid[k][i][j];
                    }
            double globalMaxF = (double)globalMax;
            double globalMinF = (double)globalMin;
            //MessageBox.Show("min = "+globalMin+" MAX = "+globalMax);
            for (int k = 0; k < numOfLevels; k++)
                for (int i = 0; i < heights[k]; i++)
                    for (int j = 0; j < widths[k]; j++)
                    {
                        normPyramid[k][i][j] = ((double)pyramid[k][i][j] - globalMinF) / (globalMaxF - globalMinF);                        
                    }
            return normPyramid;
        }

        private double[][][] getNormPyramid(ref double[][][] pyramid, ref int[] heights, ref int[] widths,
           int numOfLevels)
        {
            double[][][] normPyramid = new double[numOfLevels][][];
            for (int k = 0; k < numOfLevels; k++)
                normPyramid[k] = new double[heights[k]][];
            for (int k = 0; k < numOfLevels; k++)
                for (int i = 0; i < heights[k]; i++)
                    normPyramid[k][i] = new double[widths[k]];
            double globalMax = pyramid[0][0][0];
            double globalMin = pyramid[0][0][0];
            for (int k = 0; k < numOfLevels; k++)
                for (int i = 0; i < heights[k]; i++)
                    for (int j = 0; j < widths[k]; j++)
                    {
                        if (globalMin > pyramid[k][i][j])
                            globalMin = pyramid[k][i][j];
                        if (globalMax < pyramid[k][i][j])
                            globalMax = pyramid[k][i][j];
                    }
            //MessageBox.Show("min = "+globalMin+" MAX = "+globalMax);
            for (int k = 0; k < numOfLevels; k++)
                for (int i = 0; i < heights[k]; i++)
                    for (int j = 0; j < widths[k]; j++)
                    {
                        normPyramid[k][i][j] = (pyramid[k][i][j] - globalMin) / (globalMax - globalMin);
                    }
            return normPyramid;
        }
        private double[][] getNormMap(ref double[][] map, int height, int width)
        {
            double[][] normMap = new double[height][];
                        
            for (int i = 0; i < height; i++)
                normMap[i] = new double[width];

            double globalMax = map[0][0];
            double globalMin = map[0][0];
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    if (globalMin > map[i][j])
                        globalMin = map[i][j];
                    if (globalMax < map[i][j])
                        globalMax = map[i][j];
                }
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    normMap[i][j] = (map[i][j] - globalMin) / (globalMax - globalMin);
                }            
                
            return normMap;
        }
        private double[][] getNormMap2(ref double[][] map, int height, int width, 
            double globalMin, double globalMax)
        {
            double[][] normMap = new double[height][];

            for (int i = 0; i < height; i++)
                normMap[i] = new double[width];
            
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    normMap[i][j] = (map[i][j] - globalMin) / (globalMax - globalMin);
                }

            return normMap;
        }
        private double maxLO(double [][] filter, int filterHeight, int filterWidth)
        {
            double max = 0;
            for (int i = 0; i < filterHeight; i++)
            {
                for (int j = 0; j < filterWidth; j++)
                    if (filter[i][j]>0)
                        max+=255.0*filter[i][j];
            }
            return max;
        }
        
        private double[] getMultiplicationFactors(ref double[][][] pyramid, ref int[] height, ref int[] width, int numOfLevels,
            ref double[] localMaxima) 
        {
            localMaxima = new double[numOfLevels];
            double [] mfactors = new double[numOfLevels];
            //globalMaxima=pyramid[0][0][0];
            double sumOfMax = 0;
            for (int k = 0; k < numOfLevels; k++)
            {
                localMaxima[k] = pyramid[k][0][0];
                for (int i = 0; i < height[k]; i++)
                    for (int j = 0; j < width[k]; j++)
                    {                        
                        if (localMaxima[k] < pyramid[k][i][j])
                            localMaxima[k] = pyramid[k][i][j];
                    }
                sumOfMax += localMaxima[k];
            }
            double avgMax = (sumOfMax / (double)numOfLevels);
            for (int i = 0; i < numOfLevels; i++)
                mfactors[i] = (localMaxima[i] - avgMax) * (localMaxima[i] - avgMax);
            return mfactors;
        }
        private double[][][] PyramidScaling(ref double[][][] pyramid, int numOfLevels, 
            ref int[] heights, ref int[] widths, ref double[] factors)
        {
            double[][][] newPyramid = new double[numOfLevels][][];
            for(int k=0; k<numOfLevels; k++)
                newPyramid[k] = new double[heights[k]][];
            for(int k=0; k<numOfLevels; k++)
                for(int i=0; i<heights[k]; i++)
                    newPyramid[k][i] = new double[widths[k]];

            for (int k=0; k<numOfLevels; k++)
                for (int i=0; i<heights[k]; i++)
                    for (int j = 0; j < widths[k]; j++)
                    {
                        newPyramid[k][i][j]=pyramid[k][i][j]*factors[k];
                    }
            return newPyramid;
        }

        private double[][] getConspiciencyMap(ref double[][][] pyramid, ref int[] heights, ref int[] widths, int numOfLevels)
        {
            int finalHeight = heights[0];
            int finalWidth = widths[0];

            double[][] conspiciencyMap = new double[finalHeight][];
            for (int i = 0; i < finalHeight; i++)
                conspiciencyMap[i] = new double[finalWidth];

            for (int k = 0; k < numOfLevels; k++)
            {

                int stretchingFactor = (int)(finalHeight / heights[k]);
                double[][] strMatrix = Matrix.MatrixStretching(pyramid[k], heights[k], widths[k], stretchingFactor);
                for (int i=0; i<finalHeight; i++)
                    for (int j = 0; j < finalWidth; j++)
                    {
                        conspiciencyMap[i][j]+=strMatrix[i][j];
                    }
            }
            return conspiciencyMap;
        }

        private double[][] getConspiciencyMap2(ref int[][][] featureMaps, int numOfFeatureMaps, ref int[]heights, ref int[]widths)
        {            
            int conspMapHeight = featureMapHeights[0];
            int conspMapWidth = featureMapWidths[0];

            double[][][] normFeatureMaps = getNormPyramid(ref featureMaps, ref heights,
                ref widths, numOfFeatureMaps);
            //scaledPyramid = normIntensityFeatureMaps;

            double[] localMaxima = new double[1];
            double[] factors = getMultiplicationFactors(ref normFeatureMaps, ref heights,
                ref widths, numOfFeatureMaps, ref localMaxima);
            double[][][] scaledPyramid = PyramidScaling(ref normFeatureMaps, numOfFeatureMaps,
                ref heights, ref widths, ref factors);

            //int[][][] scaledPyramid = intensityFeatureMaps;
            double [][] conspiciencyMap = getConspiciencyMap(ref scaledPyramid, ref heights,
                ref widths, numOfFeatureMaps);
            return conspiciencyMap;
        }

        private double[][] getConspiciencyMap2(ref double[][][] featureMaps, int numOfFeatureMaps, ref int[] heights, ref int[] widths)
        {
            int conspMapHeight = featureMapHeights[0];
            int conspMapWidth = featureMapWidths[0];

            double[][][] normFeatureMaps = getNormPyramid(ref featureMaps, ref heights,
                ref widths, numOfFeatureMaps);
            //scaledPyramid = normIntensityFeatureMaps;

            double[] localMaxima = new double[1];
            double[] factors = getMultiplicationFactors(ref normFeatureMaps, ref heights,
                ref widths, numOfFeatureMaps, ref localMaxima);
            double[][][] scaledPyramid = PyramidScaling(ref normFeatureMaps, numOfFeatureMaps,
                ref heights, ref widths, ref factors);

            //int[][][] scaledPyramid = intensityFeatureMaps;
            double[][] conspiciencyMap = getConspiciencyMap(ref scaledPyramid, ref heights,
                ref widths, numOfFeatureMaps);
            return conspiciencyMap;
        }

        private void getShiftsForMatrix(int height, int width, int newHeight,
            int newWidth, ref int leftShift, ref int rightShift, ref int topShift, ref int bottomShift)
        {            
            int dHeight = newHeight - height;
            int dWidth = newWidth - width;
            rightShift = dWidth / 2;
            leftShift = dWidth - rightShift;
            topShift = dHeight / 2;
            bottomShift = dHeight - topShift;            
        }

        private void LoadImage()
        {
            int imageHeight = 1;
            int imageWidth = 1;
            //int numOfPoints = 1;
            //double[][] imageData;
            openFileDialog1.Filter = "Bitmap Files|*.bmp";
            openFileDialog1.FileName = "";
            filters = new double[numOfFilters][][];
            filters[0] = Filtering.getGaborfilter(filterSize, filterSize, 0);//Filtering.getDOGfilter(filterSize, filterSize, 0, 1.2);
            filters[1] = Filtering.getGaborfilter(filterSize, filterSize, Math.PI / 4);//Filtering.getDOGfilter(filterSize, filterSize, Math.PI/4, 1.2);
            filters[2] = Filtering.getGaborfilter(filterSize, filterSize, Math.PI / 2);//Filtering.getDOGfilter(filterSize, filterSize, Math.PI / 2, 1.2);
            filters[3] = Filtering.getGaborfilter(filterSize, filterSize, 3 * Math.PI / 4);//Filtering.getDOGfilter(filterSize, filterSize, 3*Math.PI / 4, 1.2);

            double maxFilter0 = maxLO(filters[0], filterSize, filterSize);
            //double maxFilter1 = maxLO(filters[1], filterSize, filterSize);

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                redComponents = new int[1][];
                greenComponents = new int[1][];
                blueComponents = new int[1][];
                yellowComponents = new int[1][];
                intensityMatrix = new int[1][];

                Bitmap newBitmap = new Bitmap(openFileDialog1.FileName);

                colorData = new int[1][];
                ImageImport.loadImage(newBitmap, ref colorData, ref imageHeight, ref imageWidth);
                Processings.getIndependentColorChannels(colorData, imageHeight, imageWidth, ref redComponents,
                    ref greenComponents, ref blueComponents);
                intensityMatrix = Processings.getIntensityMatrix(redComponents, greenComponents, blueComponents,
                    imageHeight, imageWidth);

                //Processings.colorNormalization(ref intensityMatrix, ref redComponents, ref greenComponents,
                //    ref blueComponents, imageHeight, imageWidth);
                intensityMatrix = Processings.cutByThreshold(intensityMatrix, imageHeight, imageWidth, (float)0.1);
                
                Bitmap tunedBitmap = Visualization.getColorImage(redComponents, greenComponents, blueComponents,
                    imageHeight, imageWidth);
                
                int[][] redChannel = new int[1][];
                int[][] greenChannel = new int[1][];
                int[][] blueChannel = new int[1][];
                int[][] yellowChannel = new int[1][];

                Processings.getTunedChannels(redComponents, greenComponents, blueComponents, intensityMatrix, 
                    imageHeight, imageWidth, 
                    ref redChannel, ref greenChannel, ref blueChannel, ref yellowChannel);
                
                //intensityMatrix = Processings.getIntensityMatrix(redChannel, greenChannel, blueChannel,
                //    imageHeight, imageWidth);

                levelHeights = new int[1];
                levelWidths = new int[1];
                
                int[] expandedHeights = new int[1];
                int[] expandedWidths = new int[1];
                int[][][] expandedPyramid = new int[1][][];

                redMaps = Pyramid.GaussPyramidCreation(imageHeight, imageWidth, numOfLevels,
                    ref levelHeights, ref levelWidths, ref expandedHeights, ref expandedWidths,
                    ref expandedPyramid);
                Pyramid.GaussPyramidFilling(redChannel, numOfLevels, levelHeights, levelWidths,
                    expandedHeights, expandedWidths, ref redMaps, ref expandedPyramid);
                blueMaps = Pyramid.GaussPyramidCreation(levelHeights, levelWidths, numOfLevels);
                Pyramid.GaussPyramidFilling(blueChannel, numOfLevels, levelHeights, levelWidths,
                    expandedHeights, expandedWidths, ref blueMaps, ref expandedPyramid);
                greenMaps = Pyramid.GaussPyramidCreation(levelHeights, levelWidths, numOfLevels);
                Pyramid.GaussPyramidFilling(greenChannel, numOfLevels, levelHeights, levelWidths,
                    expandedHeights, expandedWidths, ref greenMaps, ref expandedPyramid);
                
                yellowMaps = Pyramid.GaussPyramidCreation(levelHeights, levelWidths, numOfLevels);
                Pyramid.GaussPyramidFilling(yellowChannel, numOfLevels, levelHeights, levelWidths,
                    expandedHeights, expandedWidths, ref yellowMaps, ref expandedPyramid);

                intensityMaps = Pyramid.GaussPyramidCreation(levelHeights, levelWidths, numOfLevels);
                Pyramid.GaussPyramidFilling(intensityMatrix, numOfLevels, levelHeights, levelWidths,
                    expandedHeights, expandedWidths, ref intensityMaps, ref expandedPyramid);

                floatIntensityMaps = new double[numOfLevels][][];
                for (int i = 0; i < numOfLevels; i++)
                    floatIntensityMaps[i] = new double[levelHeights[i]][];

                for (int i = 0; i < numOfLevels; i++)
                    for (int j = 0; j < levelHeights[i]; j++ )
                        floatIntensityMaps[i][j] = new double[levelWidths[i]];

                for (int i = 0; i < numOfLevels; i++)
                    for (int j = 0; j < levelHeights[i]; j++)
                        for (int k = 0; k < levelWidths[i]; k++)
                            floatIntensityMaps[i][j][k] = (double)intensityMaps[i][j][k];

                LOMaps = new double[numOfFilters][][][];                
                for (int i = 0; i < numOfFilters; i++)                
                    LOMaps[i] = new double[numOfLevels][][];

                //Datatypes-?
                
                for (int i = 0; i < numOfFilters; i++)
                    for (int j = 0; j < numOfLevels; j++)
                        LOMaps[i][j] = Filtering.FilteringBySelectedFilter(ref floatIntensityMaps[j],
                            ref filters[i], levelHeights[j], levelWidths[j], filterSize, filterSize);
                /**/

                comboBox2.Items.Clear();
                for (int i = 0; i < numOfLevels; i++)
                {
                    comboBox2.Items.Add("Level:" + i + " H = " + levelHeights[i] + " W = " + levelWidths[i]);
                }
                comboBox2.SelectedIndex = 0;
                
                int [] centerIndices = new int[1];
                int [] surroundingsIndices = new int[1];
                int numOfFeatureMaps = 0;
                Pyramid.indicesDifference(numOfLevels, ref centerIndices, ref surroundingsIndices, 
                    ref numOfFeatureMaps);

                //for (int i = 0; i < numOfFeatureMaps; i++)
                //{
                //    listBox1.Items.Add(centerIndices[i] + " " + surroundingsIndices[i]);
                //}
                
                featureMapHeights = new int[1];
                featureMapWidths = new int[1];
                intensityFeatureMaps = Pyramid.FeatureMapsBuilding(intensityMaps, levelHeights, levelWidths,
                    centerIndices, surroundingsIndices, numOfFeatureMaps, ref featureMapHeights,
                    ref featureMapWidths);

                for (int i = 0; i < numOfFeatureMaps; i++ )
                {
                    listBox1.Items.Add("Level ID = "+i+" "+featureMapHeights[i]+" "+featureMapWidths[i]);
                }

                saliencyMapHeight = featureMapHeights[0];
                saliencyMapWidth = featureMapWidths[0];
                /*
                double[][][] normIntensityFeatureMaps = getNormPyramid(ref intensityFeatureMaps, ref featureMapHeights,
                    ref featureMapWidths, numOfFeatureMaps);
                //scaledPyramid = normIntensityFeatureMaps;

                double[] localMaxima = new double[1];
                double[] intensityMFactors = getMultiplicationFactors(ref normIntensityFeatureMaps, ref featureMapHeights,
                    ref featureMapWidths, numOfFeatureMaps, ref localMaxima);
                double[][][] scaledIFPyramid = PyramidScaling(ref normIntensityFeatureMaps, numOfFeatureMaps,
                    ref featureMapHeights, ref featureMapWidths, ref intensityMFactors);
                
                //int[][][] scaledPyramid = intensityFeatureMaps;
                intensityConspiciencyMap = getConspiciencyMap(ref scaledIFPyramid, ref featureMapHeights,
                    ref featureMapWidths, numOfFeatureMaps);
                */
                intensityConspiciencyMap = getConspiciencyMap2(ref intensityFeatureMaps, numOfFeatureMaps,
                    ref featureMapHeights, ref featureMapWidths);

                RGFeatureMaps = Processings.ColorFeatureMapsBuilding(redMaps, greenMaps, levelHeights, levelWidths,
                    centerIndices, surroundingsIndices, numOfFeatureMaps);

                RGConspiciencyMap = getConspiciencyMap2(ref RGFeatureMaps, numOfFeatureMaps, ref featureMapHeights,
                    ref featureMapWidths);               
                
                BYFeatureMaps = Processings.ColorFeatureMapsBuilding(blueMaps, yellowMaps, levelHeights, levelWidths,
                    centerIndices, surroundingsIndices, numOfFeatureMaps);
                              
                BYConspiciencyMap = getConspiciencyMap2(ref BYFeatureMaps, numOfFeatureMaps, ref featureMapHeights,
                    ref featureMapWidths);

                LOFeatureMaps = new double[numOfFilters][][][];

                for (int k = 0; k < numOfFilters; k++)
                    LOFeatureMaps[k] = Pyramid.FeatureMapsBuilding2(LOMaps[k], levelHeights, levelWidths,
                        centerIndices, surroundingsIndices, numOfFeatureMaps);

                LOConspiciencyMap = new double[numOfFilters][][];

                for (int k = 0; k < numOfFilters; k++)
                    LOConspiciencyMap[k] = getConspiciencyMap2(ref LOFeatureMaps[k], numOfFeatureMaps, ref featureMapHeights,
                        ref featureMapWidths);
                saliencyMap = new double[saliencyMapHeight][];
                for (int k = 0; k < saliencyMapHeight; k++)
                    saliencyMap[k] = new double[saliencyMapWidth];

                double[][] colorSaliencyMap = Matrix.MatrixSum(ref RGConspiciencyMap, ref BYConspiciencyMap,
                    saliencyMapHeight, saliencyMapWidth);
                double[][] LOSaliencyMap = Matrix.MatrixSum4(ref LOConspiciencyMap[0], ref LOConspiciencyMap[1],
                    ref LOConspiciencyMap[2], ref LOConspiciencyMap[3], saliencyMapHeight, saliencyMapWidth);

                //double[][] normIntensitySaliency = getNormMap(ref intensityConspiciencyMap, saliencyMapHeight, 
                //    saliencyMapWidth);
                //double[][] normColorSaliency = getNormMap(ref colorSaliencyMap, saliencyMapHeight, 
                //    saliencyMapWidth);
                //double[][] normLOSaliency = getNormMap(ref LOSaliensyMap, saliencyMapHeight, 
                //    saliencyMapWidth);
                double[][] normIntensitySaliency = getNormMap2(ref intensityConspiciencyMap, saliencyMapHeight,
                    saliencyMapWidth, 0, 3*255);
                double[][] normColorSaliency = getNormMap2(ref colorSaliencyMap, saliencyMapHeight,
                    saliencyMapWidth, 0, 6 * 255);
                double[][] normLOSaliency = getNormMap2(ref LOSaliencyMap, saliencyMapHeight,
                    saliencyMapWidth, 0, 4 * maxFilter0);

                saliencyMap = Matrix.MatrixSum3(ref normIntensitySaliency, ref normColorSaliency, ref normLOSaliency,
                    saliencyMapHeight, saliencyMapWidth);
                    //

                    //for (int i = 0; i < numOfFeatureMaps; i++)
                    //{
                    //    listBox1.Items.Add(featureMapHeights[i] + " " + featureMapWidths[i]);
                    //}
                comboBox1.SelectedIndex = 0;
                comboBox4.Items.Clear();

                for (int i = 0; i < numOfFeatureMaps; i++)
                {
                    comboBox4.Items.Add("Diff:" + centerIndices[i]+" - "+surroundingsIndices[i]);
                }
                comboBox3.SelectedIndex = 0;
                comboBox4.SelectedIndex = 0;
                
                pictureBox1.Image = newBitmap;
                displayCheck = true;
            }
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox3.SelectedIndex)
            {
                case 0:
                    {
                        if (displayCheck)
                            if (comboBox4.SelectedIndex >= 0)
                            {
                                Bitmap newBitmap = Visualization.matrixOutput(
                                    intensityFeatureMaps[comboBox4.SelectedIndex],
                                    featureMapHeights[comboBox4.SelectedIndex],
                                    featureMapWidths[comboBox4.SelectedIndex],
                                    false, 0);
                                pictureBox2.Image = newBitmap;
                            }
                        break;
                    }
                case 1:
                    {
                        if (displayCheck)
                            if (comboBox4.SelectedIndex >= 0)
                            {
                                Bitmap newBitmap = Visualization.matrixOutput(
                                    RGFeatureMaps[comboBox4.SelectedIndex],
                                    featureMapHeights[comboBox4.SelectedIndex],
                                    featureMapWidths[comboBox4.SelectedIndex],
                                    false, 0);
                                pictureBox2.Image = newBitmap;
                            }
                        break;
                    }
                case 2:
                    {
                        if (displayCheck)
                            if (comboBox4.SelectedIndex >= 0)
                            {
                                Bitmap newBitmap = Visualization.matrixOutput(
                                    BYFeatureMaps[comboBox4.SelectedIndex],
                                    featureMapHeights[comboBox4.SelectedIndex],
                                    featureMapWidths[comboBox4.SelectedIndex],
                                    false, 0);
                                pictureBox2.Image = newBitmap;
                            }
                        break;
                    }
                case 3:
                    {
                        if (displayCheck)
                            if (comboBox4.SelectedIndex >= 0)
                            {
                                Bitmap newBitmap = Visualization.matrixOutput(
                                    LOFeatureMaps[0][comboBox4.SelectedIndex],
                                    featureMapHeights[comboBox4.SelectedIndex],
                                    featureMapWidths[comboBox4.SelectedIndex],
                                    true, 0);
                                pictureBox2.Image = newBitmap;
                            }
                        break;
                    }
                case 4:
                    {
                        if (displayCheck)
                            if (comboBox4.SelectedIndex >= 0)
                            {
                                Bitmap newBitmap = Visualization.matrixOutput(
                                    LOFeatureMaps[1][comboBox4.SelectedIndex],
                                    featureMapHeights[comboBox4.SelectedIndex],
                                    featureMapWidths[comboBox4.SelectedIndex],
                                    true, 0);
                                pictureBox2.Image = newBitmap;
                            }
                        break;
                    }
                case 5:
                    {
                        if (displayCheck)
                            if (comboBox4.SelectedIndex >= 0)
                            {
                                Bitmap newBitmap = Visualization.matrixOutput(
                                    LOFeatureMaps[2][comboBox4.SelectedIndex],
                                    featureMapHeights[comboBox4.SelectedIndex],
                                    featureMapWidths[comboBox4.SelectedIndex],
                                    true, 0);
                                pictureBox2.Image = newBitmap;
                            }
                        break;
                    }
                case 6:
                    {
                        if (displayCheck)
                            if (comboBox4.SelectedIndex >= 0)
                            {
                                Bitmap newBitmap = Visualization.matrixOutput(
                                    LOFeatureMaps[3][comboBox4.SelectedIndex],
                                    featureMapHeights[comboBox4.SelectedIndex],
                                    featureMapWidths[comboBox4.SelectedIndex],
                                    true, 0);
                                pictureBox2.Image = newBitmap;
                            }
                        break;
                    }
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    {
                        if (displayCheck)
                            if (comboBox2.SelectedIndex >= 0)
                            {
                                Bitmap newBitmap = Visualization.matrixOutput(intensityMaps[comboBox2.SelectedIndex],
                                levelHeights[comboBox2.SelectedIndex], levelWidths[comboBox2.SelectedIndex],
                                false, 0);
                                pictureBox2.Image = newBitmap;
                            }
                        break;
                    }
                case 1:
                    {
                        if (displayCheck)
                            if (comboBox2.SelectedIndex >= 0)
                            {
                                Bitmap newBitmap = Visualization.matrixOutput(redMaps[comboBox2.SelectedIndex],
                                levelHeights[comboBox2.SelectedIndex], levelWidths[comboBox2.SelectedIndex],
                                false, 1);
                                pictureBox2.Image = newBitmap;
                            }
                        break;
                    }
                case 2:
                    {
                        if (displayCheck)
                            if (comboBox2.SelectedIndex >= 0)
                            {
                                Bitmap newBitmap = Visualization.matrixOutput(greenMaps[comboBox2.SelectedIndex],
                                levelHeights[comboBox2.SelectedIndex], levelWidths[comboBox2.SelectedIndex],
                                false, 2);
                                pictureBox2.Image = newBitmap;
                            }
                        break;
                    }
                case 3:
                    {
                        if (displayCheck)
                            if (comboBox2.SelectedIndex >= 0)
                            {
                                Bitmap newBitmap = Visualization.matrixOutput(blueMaps[comboBox2.SelectedIndex],
                                levelHeights[comboBox2.SelectedIndex], levelWidths[comboBox2.SelectedIndex],
                                false, 3);
                                pictureBox2.Image = newBitmap;
                            }
                        break;
                    }
                case 4:
                    {
                        if (displayCheck)
                            if (comboBox2.SelectedIndex >= 0)
                            {
                                Bitmap newBitmap = Visualization.matrixOutput(yellowMaps[comboBox2.SelectedIndex],
                                levelHeights[comboBox2.SelectedIndex], levelWidths[comboBox2.SelectedIndex],
                                false, 1);
                                pictureBox2.Image = newBitmap;
                            }
                        break;
                    }
                    /**/
                case 5:
                    {
                        if (displayCheck)
                            if (comboBox2.SelectedIndex >= 0)
                            {
                                Bitmap newBitmap = Visualization.matrixOutput(LOMaps[0][comboBox2.SelectedIndex],
                                levelHeights[comboBox2.SelectedIndex], levelWidths[comboBox2.SelectedIndex],
                                false, 1);
                                pictureBox2.Image = newBitmap;
                            }
                        break;
                    }
                case 6:
                    {
                        if (displayCheck)
                            if (comboBox2.SelectedIndex >= 0)
                            {
                                Bitmap newBitmap = Visualization.matrixOutput(LOMaps[1][comboBox2.SelectedIndex],
                                levelHeights[comboBox2.SelectedIndex], levelWidths[comboBox2.SelectedIndex],
                                false, 1);
                                pictureBox2.Image = newBitmap;
                            }
                        break;
                    }
                case 7:
                    {
                        if (displayCheck)
                            if (comboBox2.SelectedIndex >= 0)
                            {
                                Bitmap newBitmap = Visualization.matrixOutput(LOMaps[2][comboBox2.SelectedIndex],
                                levelHeights[comboBox2.SelectedIndex], levelWidths[comboBox2.SelectedIndex],
                                false, 1);
                                pictureBox2.Image = newBitmap;
                            }
                        break;
                    }
                case 8:
                    {
                        if (displayCheck)
                            if (comboBox2.SelectedIndex >= 0)
                            {
                                Bitmap newBitmap = Visualization.matrixOutput(LOMaps[3][comboBox2.SelectedIndex],
                                levelHeights[comboBox2.SelectedIndex], levelWidths[comboBox2.SelectedIndex],
                                false, 1);
                                pictureBox2.Image = newBitmap;
                            }
                        break;
                    }
                    /**/

            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox2_SelectedIndexChanged(sender, e);
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox4_SelectedIndexChanged(sender, e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Bitmap files (*.bmp)|*.bmp|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox2.Image.Save(saveFileDialog1.FileName);
            }
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox5.SelectedIndex)
            {
                case 0:
                    {
                        if (displayCheck)
                        {

                            //for (int i=0; i<20; i++)
                            Bitmap newBitmap = Visualization.matrixOutput(intensityConspiciencyMap,//intensityFeatureMaps[0],
                                saliencyMapHeight, saliencyMapWidth,
                                true, 0);
                            pictureBox2.Image = newBitmap;
                        }
                        break;
                    }
                case 1:
                    {
                        if (displayCheck)
                        {
                            //for (int i=0; i<20; i++)
                            Bitmap newBitmap = Visualization.matrixOutput(RGConspiciencyMap,//intensityFeatureMaps[0],
                                saliencyMapHeight, saliencyMapWidth,
                                true, 0);
                            pictureBox2.Image = newBitmap;
                        }
                        break;
                    }
                case 2:
                    {
                        if (displayCheck)
                        {
                            //for (int i=0; i<20; i++)
                            Bitmap newBitmap = Visualization.matrixOutput(BYConspiciencyMap,//intensityFeatureMaps[0],
                                saliencyMapHeight, saliencyMapWidth,
                                true, 0);
                            pictureBox2.Image = newBitmap;
                        }
                        break;
                    }
                case 3:
                    {
                        if (displayCheck)
                        {
                            //for (int i=0; i<20; i++)
                            Bitmap newBitmap = Visualization.matrixOutput(LOConspiciencyMap[0],//intensityFeatureMaps[0],
                                saliencyMapHeight, saliencyMapWidth,
                                true, 0);
                            pictureBox2.Image = newBitmap;
                        }
                        break;
                    }
                case 4:
                    {
                        if (displayCheck)
                        {
                            //for (int i=0; i<20; i++)
                            Bitmap newBitmap = Visualization.matrixOutput(LOConspiciencyMap[1],//intensityFeatureMaps[0],
                                saliencyMapHeight, saliencyMapWidth,
                                true, 0);
                            pictureBox2.Image = newBitmap;
                        }
                        break;
                    }
                case 5:
                    {
                        if (displayCheck)
                        {
                            //for (int i=0; i<20; i++)
                            Bitmap newBitmap = Visualization.matrixOutput(LOConspiciencyMap[2],//intensityFeatureMaps[0],
                                saliencyMapHeight, saliencyMapWidth,
                                true, 0);
                            pictureBox2.Image = newBitmap;
                        }
                        break;
                    }
                case 6:
                    {
                        if (displayCheck)
                        {
                            //for (int i=0; i<20; i++)
                            Bitmap newBitmap = Visualization.matrixOutput(LOConspiciencyMap[3],//intensityFeatureMaps[0],
                                saliencyMapHeight, saliencyMapWidth,
                                true, 0);
                            pictureBox2.Image = newBitmap;
                        }
                        break;
                    }
            }
                
        }

        private void button2_Click(object sender, EventArgs e)
        {
          /*  int imageHeight = 1;
            int imageWidth = 1;
            openFileDialog1.Filter = "Bitmap Files|*.bmp";
            openFileDialog1.FileName = "";
            filters = new double[numOfFilters][][];
            filters[0] = Filtering.getDOGfilter(filterSize, filterSize, 0, 1.2);
            filters[1] = Filtering.getDOGfilter(filterSize, filterSize, Math.PI/4, 1.2);
            filters[2] = Filtering.getDOGfilter(filterSize, filterSize, Math.PI / 2, 1.2);
            filters[3] = Filtering.getDOGfilter(filterSize, filterSize, 3*Math.PI / 4, 1.2);

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                redComponents = new int[1][];
                greenComponents = new int[1][];
                blueComponents = new int[1][];
                yellowComponents = new int[1][];
                intensityMatrix = new int[1][];

                Bitmap newBitmap = new Bitmap(openFileDialog1.FileName);

                colorData = new int[1][];
                ImageImport.loadImage(newBitmap, ref colorData, ref imageHeight, ref imageWidth);
                Processings.getIndependentColorChannels(colorData, imageHeight, imageWidth, ref redComponents,
                    ref greenComponents, ref blueComponents);
                intensityMatrix = Processings.getIntensityMatrix(redComponents, greenComponents, blueComponents,
                    imageHeight, imageWidth);

                newBitmap = Visualization.matrixOutput(intensityMatrix,
                       imageHeight, imageWidth,
                       false, 0);
                pictureBox1.Image = newBitmap;

                int globalMax = Matrix.MaximumOfMatrix(intensityMatrix, imageHeight, imageWidth);
                int globalMin = Matrix.MinOfMatrix(intensityMatrix, imageHeight, imageWidth);

                double [][]scaled = new double[imageHeight][];
                for (int i=0; i<imageHeight; i++)
                    scaled[i] = new double[imageWidth];
                //
                double globalMaxF = (double)globalMax;
                double globalMinF = (double)globalMin;

                for (int i = 0; i < imageHeight; i++)
                    for (int j = 0; j < imageWidth; j++)
                    {
                        scaled[i][j] = ((double)intensityMatrix[i][j]-globalMinF)/(globalMaxF-globalMinF);
                        //((double)pyramid[k][i][j] - globalMinF) / globalMaxF;                        
                    }

                //
                Bitmap newBitmap2 = Visualization.matrixOutput(scaled,
                       imageHeight, imageWidth,
                       true, 0);
                pictureBox2.Image = newBitmap2;
            }*/
            int matrixHeight = 1000;
            int matrixWeight = 1200;
            int []heights = new int[1];
            int[] widths = new int[1];
            int[] exHeights = new int[1];
            int[] exWidths = new int[1];
            int[] correctHeights = new int[1];
            int[] correctWidths = new int[1];
            int[][][] exPyramid = new int[1][][];
            int[][][] gaussPyramid = GaussPyramidCreation2(matrixHeight, matrixWeight, numOfLevels,
                ref heights, ref widths, ref exHeights, ref exWidths, ref exPyramid);
        /*    for (int i = 1; i < numOfLevels; i++)
            {
                listBox1.Items.Add("Level = "+i);
                listBox1.Items.Add("D = 1");
                //int factor1 = 2;
                listBox1.Items.Add("H = "+heights[i]+" W = "+widths[i]+" H0 = "+heights[i-1]+" W0 = "+widths[i-1]);
                fixSizesForMatrix(ref gaussPyramid[i], heights[i]*2, widths[i]*2, heights[i - 1], widths[i - 1]);
                listBox1.Items.Add("D = 2");
                if (i >= 2)
                {
                    //int factor2 = 4;
                    fixSizesForMatrix(ref gaussPyramid[i], heights[i]*4, widths[i]*4, heights[i - 2], widths[i - 2]);
                    listBox1.Items.Add("H = " + heights[i] + " W = " + widths[i] + " H1 = " + heights[i - 2] + 
                        " W1 = " + widths[i - 2]);
                }
                    
                listBox1.Items.Add("D = 3");
                if (i >= 3)
                {
                    fixSizesForMatrix(ref gaussPyramid[i], heights[i]*8, widths[i]*8, heights[i - 3], widths[i - 3]);
                    listBox1.Items.Add("H = " + heights[i] + " W = " + widths[i] + " H2 = " + heights[i - 3] +
                        " W2 = " + widths[i - 3]);
                }
                
            }*/
            int height1 = 10;
            int width1 = 10;

            int height2 = 6;
            int width2 = 9;
            int[][] matrix1 = new int[height1][];
            for(int i=0; i<height1; i++)
                matrix1[i] = new int[width1];

            int[][] matrix2 = new int[height2][];
            for (int i = 0; i < height2; i++)
                matrix2[i] = new int[width2];

            for (int i = 0; i < height1; i++)
                for (int j = 0; j < width1; j++)
                    matrix1[i][j] = 2;

            for (int i = 0; i < height2; i++)
                for (int j = 0; j < width2; j++)
                    matrix2[i][j] = 1;

            int[][] result = new int[height1][];
            for (int i = 0; i < height1; i++)
                result[i] = new int[width1];

            //int initialYCoordinate = i + fi - shiftY;
            //int initialXCoordinate = j + fj - shiftX;
            int leftShift = 0;
            int rightShift = 0;
            int topShift = 0;
            int bottomShift = 0;
            getShiftsForMatrix(height2, width2, height1, width1, ref leftShift, ref rightShift, 
                ref topShift, ref bottomShift);

            int borderX = leftShift + width2;
            int borderY = topShift + height2;

            int positionHeight = height2 + height2 + topShift;
            int positionWidth = width2 + width2 + leftShift;

            for (int i = 0; i < height1; i++)
            {
                String str = "";
                for (int j = 0; j < width1; j++)
                {
                    int positionX = -1;
                    int positionY = -1;
                    if (j < leftShift)
                    {
                        positionX = leftShift - j - 1;
                        //listBox1.Items.Add("posx "+ positionX);
                    }
                    else if (j < borderX)
                    {
                        positionX = j - leftShift;
                    }
                    else
                    {
                        positionX = positionWidth - j - 1;
                    }

                    if (i < topShift)
                    {
                        positionY = topShift - i - 1;
                    }
                    else if (i < borderY)
                    {
                        positionY = i - topShift;
                    }
                    else
                    {
                        positionY = positionHeight - i - 1;
                    }

                    if ((positionX >= 0) && (positionY >= 0))
                    {
                        result[i][j] = matrix1[i][j] - matrix2[positionY][positionX];
                        
                    }
                    str += " " + result[i][j];
                }
                listBox1.Items.Add(str);
            }
                
            //for(int i=0; i<numOfLevels; i++)
            //    listBox1.Items.Add(i+" : "+heights[i]+" "+widths[i]+" >>> "+
            //        correctHeights[i]+" "+correctWidths[i]);
        }

        private void levelSubstruction(ref int[][] topLevel, int topLevelHeight, int topLevelWidth,
            ref int[][] bottomLevel, int bottomLevelHeight, int bottomLevelWidth, ref int[][] result)
        { 
        }

        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox5.SelectedIndex >= 0)
            {
                if (displayCheck)
                {
                    Bitmap newBitmap = Visualization.matrixOutput(saliencyMap,//intensityFeatureMaps[0],
                               saliencyMapHeight, saliencyMapWidth,
                               true, 0);
                    pictureBox2.Image = newBitmap;
                }
            }
        }
    }
}
