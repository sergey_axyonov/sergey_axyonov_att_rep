﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttentionSystem
{
    class Filtering
    {
        public static double[][] getDOGfilter(int height, int width, double angle, 
            double deviation)
        {
            double [][] filter = new double[height][];
            for (int i = 0; i < height; i++)
                filter[i] = new double[width];
            int xcenter = width / 2;
            int ycenter = height / 2;
            double dev2 = deviation * deviation;

            for (int xreal=0; xreal<width; xreal++)
                for(int yreal=0; yreal<height; yreal++)
                {
                    int x = xreal - xcenter;
                    int y = yreal - ycenter;
                    double xCosQ = x * Math.Cos(angle);
                    double ySinQ = y * Math.Sin(angle);
                    filter[yreal][xreal] = Math.Pow(-xCosQ + ySinQ, 2) / (dev2 * (dev2 - 1)) *
                        Math.Exp(-(Math.Pow(xCosQ + ySinQ, 2) +
                        Math.Pow(-xCosQ + ySinQ, 2)) / (2 * dev2));
                }
            return filter;
        }
        public static double[][] getGaborfilter(int height, int width, double angle)
        {
            double[][] filter = new double[height][];
            for (int i = 0; i < height; i++)
                filter[i] = new double[width];
            int xcenter = width / 2;
            int ycenter = height / 2;
            //double dev2 = deviation * deviation;
            
            double dev = 0.0036 * Math.Pow(height, 2) + 0.35 * height + 0.18;
            double lambda = dev / 0.8;
            double gamma = 0.3;
            double phase = 0; 
            for (int xreal = 0; xreal < width; xreal++)
                for (int yreal = 0; yreal < height; yreal++)
                {
                    int x = xreal - xcenter;
                    int y = yreal - ycenter;
                    double xCosQ = x * Math.Cos(angle);
                    double ySinQ = y * Math.Sin(angle);
                    filter[yreal][xreal] = Math.Cos(2 * Math.PI * (xCosQ + ySinQ) /
                        lambda + phase) *
                        Math.Exp(-(Math.Pow(xCosQ + ySinQ, 2) +
                        Math.Pow(gamma, 2) * Math.Pow(-xCosQ + ySinQ, 2)) /
                        (2 * Math.Pow(dev, 2)));
                }
            return filter;
        }
        public static double[][] FilteringBySelectedFilter(ref double[][] inputMatrix, 
            ref double[][] filteringMatrix,
            int matrixHeight, int matrixWidth, int filterHeight, int filterWidth)
        {
            double[][] filteredMatrix = new double[matrixHeight][];
            for (int i=0; i<matrixHeight; i++)
                filteredMatrix[i] = new double [matrixWidth];

            int shiftX = (filterHeight - 1) / 2;
            int shiftY = (filterWidth - 1) / 2;

            int positionHeight = matrixHeight + matrixHeight + shiftY;
            int positionWidth = matrixWidth + matrixWidth + shiftX;

            for (int i = 0; i < matrixHeight; i++)
                for (int j = 0; j < matrixWidth; j++)
                {
                    double convolution = 0;

                    for (int fi = 0; fi < filterHeight; fi++)
                        for (int fj = 0; fj < filterWidth; fj++)
                        {
                            int initialYCoordinate = i + fi - shiftY;
                            int initialXCoordinate = j + fj - shiftX;
                            int xCoordinate = 0;
                            int yCoordinate = 0;

                            if (initialXCoordinate < 0)
                                xCoordinate = shiftX - j - fj - 1;
                            else if (initialXCoordinate < matrixWidth)
                                xCoordinate = initialXCoordinate;
                            else
                                xCoordinate = positionWidth - j - fj - 1;

                            if (initialYCoordinate < 0)
                                yCoordinate = shiftY - i - fi - 1;
                            else if (initialYCoordinate < matrixHeight)
                                yCoordinate = initialYCoordinate;
                            else
                                yCoordinate = positionHeight - i - fi - 1;

                            convolution += inputMatrix[yCoordinate][xCoordinate] * 
                                filteringMatrix[fj][fi];
                        }
                    filteredMatrix[i][j] = convolution;
                }
            //
            return filteredMatrix;
        }
    }
}
