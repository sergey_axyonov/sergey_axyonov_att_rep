﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttentionSystem
{
    class ImageImport
    {
        public static void loadImage(Bitmap bitmap, ref int[][] colorData, ref int imageHeight,
           ref int imageWidth)
        {
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(new Point(0, 0),
               bitmap.Size), ImageLockMode.ReadOnly, bitmap.PixelFormat);
            imageHeight = bitmap.Height;
            imageWidth = bitmap.Width;
            int imageSize = imageHeight * imageWidth;

            colorData = new int[imageSize][];
            for (int i = 0; i < imageSize; i++)
                colorData[i] = new int[3];

            unsafe
            {
                int byteCounts = 3;
                for (int i = 0; i < bitmap.Height; i++)
                {
                    byte* OriginalRowPtr = (byte*)bitmapData.Scan0 +
                        i * bitmapData.Stride;
                    for (int j = 0; j < bitmap.Width; j++)
                    {
                        int ColorPosition = j * byteCounts;

                        byte b = OriginalRowPtr[ColorPosition];
                        byte g = OriginalRowPtr[ColorPosition + 1];
                        byte r = OriginalRowPtr[ColorPosition + 2];

                        colorData[i * imageWidth + j][0] = r;
                        colorData[i * imageWidth + j][1] = g;
                        colorData[i * imageWidth + j][2] = b;
                    }
                }
            }

            bitmap.UnlockBits(bitmapData);
        }
    }
}
