﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttentionSystem
{
    class Visualization
    {
        public static Bitmap getColorImage(int[][] redComponents, int[][] greenComponents, int[][] blueComponents,
           int height, int width)
        {
            Bitmap newBitmap = new Bitmap(width, height,
                System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            unsafe
            {
                byte BytesCount = 3;
                BitmapData bitmapData = newBitmap.LockBits(new Rectangle(
                     new Point(0, 0), newBitmap.Size), ImageLockMode.WriteOnly,
                     newBitmap.PixelFormat);
                for (int i = 0; i < height; i++)
                {
                    byte* BitmapRowPtr = (byte*)bitmapData.Scan0 + i * bitmapData.Stride;
                    for (int j = 0; j < width; j++)
                    {
                        int ColorPosition = j * BytesCount;
                        BitmapRowPtr[ColorPosition] = (byte)blueComponents[i][j];
                        BitmapRowPtr[ColorPosition + 1] = (byte)greenComponents[i][j];
                        BitmapRowPtr[ColorPosition + 2] = (byte)redComponents[i][j];
                    }
                }
                newBitmap.UnlockBits(bitmapData);
            }
            return newBitmap;
        }

        public static Bitmap matrixOutput(int[][] matrix, int height, int width, bool scaled, byte colorOfMap)
        {
            int[][] scaledMatrix = new int[1][];
            if (scaled == true)
            {
                int maxValue = Matrix.MaximumOfMatrix(matrix, height, width);
                double factor;

                if (maxValue != 0)
                    factor = (double)255 / maxValue;
                else
                    factor = 1;
                scaledMatrix = Matrix.MatrixScaling(matrix, height, width, factor);
            }
            else
            {
                scaledMatrix = matrix;
            }

            Bitmap displayedBitmap = new Bitmap(width, height,
                System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            unsafe
            {
                byte BytesCount = 3;
                BitmapData UpdatingData = displayedBitmap.LockBits(new Rectangle(
                     new Point(0, 0), displayedBitmap.Size), ImageLockMode.WriteOnly,
                     displayedBitmap.PixelFormat);
                for (int i = 0; i < height; i++)
                {
                    byte* BitmapRowPtr = (byte*)UpdatingData.Scan0 + i * UpdatingData.Stride;
                    for (int j = 0; j < width; j++)
                    {
                        int ColorPosition = j * BytesCount;
                        switch (colorOfMap)
                        {
                            case 0:
                                {
                                    BitmapRowPtr[ColorPosition] = (byte)Math.Abs(scaledMatrix[i][j]);
                                    BitmapRowPtr[ColorPosition + 1] = (byte)Math.Abs(scaledMatrix[i][j]);
                                    BitmapRowPtr[ColorPosition + 2] = (byte)Math.Abs(scaledMatrix[i][j]);
                                    break;
                                }
                            case 1:
                                {
                                    BitmapRowPtr[ColorPosition] = 0;
                                    BitmapRowPtr[ColorPosition + 1] = 0;
                                    BitmapRowPtr[ColorPosition + 2] = (byte)Math.Abs(scaledMatrix[i][j]);
                                    break;
                                }
                            case 2:
                                {
                                    BitmapRowPtr[ColorPosition] = 0;
                                    BitmapRowPtr[ColorPosition + 1] = (byte)Math.Abs(scaledMatrix[i][j]);
                                    BitmapRowPtr[ColorPosition + 2] = 0;
                                    break;
                                }
                            case 3:
                                {
                                    BitmapRowPtr[ColorPosition] = (byte)Math.Abs(scaledMatrix[i][j]);
                                    BitmapRowPtr[ColorPosition + 1] = 0;
                                    BitmapRowPtr[ColorPosition + 2] = 0;
                                    break;
                                }
                        }

                    }
                }
                displayedBitmap.UnlockBits(UpdatingData);
            }
            return displayedBitmap;
        }
        public static Bitmap matrixOutput(double[][] matrix, int height, int width, bool scaled, byte colorOfMap)
        {
            int[][] scaledMatrix = new int[1][];
            if (scaled == true)
            {
                double maxValue = Matrix.MaximumOfMatrix(matrix, height, width);
                double factor;

                if (maxValue != 0)
                    factor = (double)255 / maxValue;
                else
                    factor = 1;
                scaledMatrix = Matrix.MatrixScaling(matrix, height, width, factor);
            }
            else
            {
                //scaledMatrix = matrix;
                //
                scaledMatrix = new int[height][];
                for (int i=0; i<height; i++)                    
                    scaledMatrix[i] = new int[width];

                for(int i=0; i<height; i++)
                    for(int j=0; j<width; j++)
                        scaledMatrix[i][j] = (int)matrix[i][j];
            }

            Bitmap displayedBitmap = new Bitmap(width, height,
                System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            unsafe
            {
                byte BytesCount = 3;
                BitmapData UpdatingData = displayedBitmap.LockBits(new Rectangle(
                     new Point(0, 0), displayedBitmap.Size), ImageLockMode.WriteOnly,
                     displayedBitmap.PixelFormat);
                for (int i = 0; i < height; i++)
                {
                    byte* BitmapRowPtr = (byte*)UpdatingData.Scan0 + i * UpdatingData.Stride;
                    for (int j = 0; j < width; j++)
                    {
                        int ColorPosition = j * BytesCount;
                        switch (colorOfMap)
                        {
                            case 0:
                                {
                                    BitmapRowPtr[ColorPosition] = (byte)Math.Abs(scaledMatrix[i][j]);
                                    BitmapRowPtr[ColorPosition + 1] = (byte)Math.Abs(scaledMatrix[i][j]);
                                    BitmapRowPtr[ColorPosition + 2] = (byte)Math.Abs(scaledMatrix[i][j]);
                                    break;
                                }
                            case 1:
                                {
                                    BitmapRowPtr[ColorPosition] = 0;
                                    BitmapRowPtr[ColorPosition + 1] = 0;
                                    BitmapRowPtr[ColorPosition + 2] = (byte)Math.Abs(scaledMatrix[i][j]);
                                    break;
                                }
                            case 2:
                                {
                                    BitmapRowPtr[ColorPosition] = 0;
                                    BitmapRowPtr[ColorPosition + 1] = (byte)Math.Abs(scaledMatrix[i][j]);
                                    BitmapRowPtr[ColorPosition + 2] = 0;
                                    break;
                                }
                            case 3:
                                {
                                    BitmapRowPtr[ColorPosition] = (byte)Math.Abs(scaledMatrix[i][j]);
                                    BitmapRowPtr[ColorPosition + 1] = 0;
                                    BitmapRowPtr[ColorPosition + 2] = 0;
                                    break;
                                }
                        }

                    }
                }
                displayedBitmap.UnlockBits(UpdatingData);
            }
            return displayedBitmap;
        }
    }
}
