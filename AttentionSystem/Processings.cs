﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttentionSystem
{
    class Processings
    {
        public static int[][] cutByThreshold(int[][] matrix, int height, int width, float percentage)
        {
            int[][] thresholdedMatrix = new int[height][];
            for (int i = 0; i < height; i++)
                thresholdedMatrix[i] = new int[width];

            int maxValue = matrix[0][0];
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                    if (matrix[i][j] > maxValue)
                        maxValue = matrix[i][j];            

            int threshold = (int)(percentage * maxValue);           
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)                                  
                    if (matrix[i][j] > threshold)
                        thresholdedMatrix[i][j] = matrix[i][j];
                    else
                        thresholdedMatrix[i][j] = 0;
                
            return thresholdedMatrix;
        }
        public static void getIndependentColorChannels(int[][] colorData, int height, int width,
           ref int[][] redComponents, ref int[][] greenComponents, ref int[][] blueComponents)
        {
            redComponents = new int[height][];
            greenComponents = new int[height][];
            blueComponents = new int[height][];
            for (int i = 0; i < height; i++)
            {
                redComponents[i] = new int[width];
                greenComponents[i] = new int[width];
                blueComponents[i] = new int[width];
            }
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    redComponents[i][j] = colorData[i * width + j][0];
                    greenComponents[i][j] = colorData[i * width + j][1];
                    blueComponents[i][j] = colorData[i * width + j][2];
                }
        }

        public static int[][] getIntensityMatrix(int[][] redComponents, int[][] greenComponents,
            int[][] blueComponents, int imageHeight, int imageWidth)
        {
            int[][] intensityMatrix = new int[imageHeight][];
            for (int i = 0; i < imageHeight; i++)
                intensityMatrix[i] = new int[imageWidth];
            for (int i = 0; i < imageHeight; i++)
                for (int j = 0; j < imageWidth; j++)
                    intensityMatrix[i][j] = (redComponents[i][j] + greenComponents[i][j] + blueComponents[i][j]) / 3;
            return intensityMatrix;
        }

        public static void getTunedChannels(int[][] redValues, int[][] greenValues, int[][] blueValues,
           int[][] tunedIntensityValues, int height, int width,
           ref int[][] redChannel, ref int[][] greenChannel, ref int[][] blueChannel, ref int[][] yellowChannel)
        {
            redChannel = new int[height][];
            greenChannel = new int[height][];
            blueChannel = new int[height][];
            yellowChannel = new int[height][];
            for (int i = 0; i < height; i++)
            {
                redChannel[i] = new int[width];
                greenChannel[i] = new int[width];
                blueChannel[i] = new int[width];
                yellowChannel[i] = new int[width];
            }
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    if (tunedIntensityValues[i][j] > 0)
                    {
                        redChannel[i][j] = redValues[i][j] - (greenValues[i][j] + blueValues[i][j]) / 2;
                        if (redChannel[i][j] < 0)
                            redChannel[i][j] = 0;
                        greenChannel[i][j] = greenValues[i][j] - (redValues[i][j] + blueValues[i][j]) / 2;
                        if (greenChannel[i][j] < 0)
                            greenChannel[i][j] = 0;
                        blueChannel[i][j] = blueValues[i][j] - (redValues[i][j] + greenValues[i][j]) / 2;
                        if (blueChannel[i][j] < 0)
                            blueChannel[i][j] = 0;
                        yellowChannel[i][j] = redValues[i][j] + greenValues[i][j] -
                            2 * (Math.Abs(redValues[i][j] - greenValues[i][j]) + blueValues[i][j]);
                        if (yellowChannel[i][j] < 0)
                            yellowChannel[i][j] = 0;
                    }
                    else
                    {
                        redChannel[i][j] = 0;
                        greenChannel[i][j] = 0;
                        blueChannel[i][j] = 0;
                        yellowChannel[i][j] = 0;
                    }
                }
        }


        public static int[][][] ColorFeatureMapsBuilding(int[][][] colorAGaussPyramid, int[][][] colorBGaussPyramid,
          int[] gaussMapHeights, int[] gaussMapWidths,
          int[] centerIndices, int[] surroundingsIndices, int numOfFeatureMaps)
        {
            int[][][] featureMaps = new int[numOfFeatureMaps][][];

            for (int i = 0; i < numOfFeatureMaps; i++)
            {
                int centerIndex = centerIndices[i];
                int surroundingsIndex = surroundingsIndices[i];

                int[][] centerMatrixA = colorAGaussPyramid[centerIndex];
                int[][] centerMatrixB = colorBGaussPyramid[centerIndex];

                int[][] surroundingsMatrixA = colorAGaussPyramid[surroundingsIndex];
                int[][] surroundingsMatrixB = colorBGaussPyramid[surroundingsIndex];

                int stretchingFactor = (int)Math.Pow(2, surroundingsIndex - centerIndex);

                int[][] centerMatrix = Matrix.MatrixSubstruction(centerMatrixA, centerMatrixB,
                    gaussMapHeights[centerIndex], gaussMapWidths[centerIndex]);
                int[][] surroundingsMatrix = Matrix.MatrixSubstruction(surroundingsMatrixB, surroundingsMatrixA,
                    gaussMapHeights[surroundingsIndex], gaussMapWidths[surroundingsIndex]);

                int[][] stretchedSurroundingsMatrix = Matrix.MatrixStretching(surroundingsMatrix,
                        gaussMapHeights[surroundingsIndex], gaussMapWidths[surroundingsIndex], stretchingFactor);

                int[][] aMatrix = Matrix.MatrixSubstruction(centerMatrix,
                        stretchedSurroundingsMatrix, gaussMapHeights[centerIndex], gaussMapWidths[centerIndex]);
                featureMaps[i] = Matrix.MatrixAbsolute(aMatrix, gaussMapHeights[centerIndex],
                    gaussMapWidths[centerIndex]);
            }
            return featureMaps;
        }
    }
}
