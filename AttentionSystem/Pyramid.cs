﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttentionSystem
{
    class Pyramid
    {
        public static int bestNumOfLevels(int height, int width, ref int[] heights, ref int[] widths)
        {
            int numOfLevels = 0;
            int minHeight = 2;
            int minWidth = 2;
            int maxNumOfLevels = 14;
            int[] lHeights = new int[maxNumOfLevels];
            lHeights[0] = height;
            int[] lWidths = new int[maxNumOfLevels];
            lWidths[0] = width;
            for (int i = 1; i < maxNumOfLevels; i++)
            {
                int factor = (int)Math.Pow(2, i);
                lHeights[i] = height / factor;
                lWidths[i] = width / factor;
            }
            int j = 0;

            while ((lHeights[j] > minHeight) && (lWidths[j] > minWidth))
            {
                j++;
                if (j == maxNumOfLevels)
                    break;
            }
            numOfLevels = j;
            for (int i = 0; i < numOfLevels; i++)
            {
                heights[i] = lHeights[i];
                widths[i] = lWidths[i];
            }

            return numOfLevels;
        }

        public static void indicesDifference(int numOfLevel,
          ref int[] centerIndices, ref int[] surroundingsIndices, ref int size)
        {
            int firstCenterIndex = 2;
            //Different levels 2-5(+3), 2-6(+4), 3-6(+3), 3-7(+4), 4-7, 4-8, etc.
            int minDifference = 3;
            int maxDifference = 4;
            int differentPerLevel = maxDifference - minDifference + 1;
            int lastCenterIndex = numOfLevel - 4 - 1;
            size = (lastCenterIndex - firstCenterIndex + 1) * differentPerLevel;
            centerIndices = new int[size];
            surroundingsIndices = new int[size];
            for (int i = firstCenterIndex; i <= lastCenterIndex; i++)
                for (int difference = minDifference; difference <= maxDifference; difference++)
                {
                    int index = (i - firstCenterIndex) * differentPerLevel + difference - minDifference;
                    centerIndices[index] = i;
                    surroundingsIndices[index] = i + difference;
                }
        }

        public static int[][][] FeatureMapsBuilding(int[][][] gaussPyramid, int[] gaussMapHeights, int[] gaussMapWidths,
            int[] centerIndices, int[] surroundingsIndices, int numOfFeatureMaps, ref int[] featureMapHeights,
            ref int[] featureMapWidths)
        {
            int[][][] featureMaps = new int[numOfFeatureMaps][][];
            featureMapHeights = new int[numOfFeatureMaps];
            featureMapWidths = new int[numOfFeatureMaps];

            for (int i = 0; i < numOfFeatureMaps; i++)
            {
                int centerIndex = centerIndices[i];
                int surroundingsIndex = surroundingsIndices[i];
                int[][] centerMatrix = gaussPyramid[centerIndex];
                int[][] surroundingsMatrix = gaussPyramid[surroundingsIndex];
                int stretchingFactor = (int)Math.Pow(2, surroundingsIndex - centerIndex);

                int[][] stretchedSurroundingsMatrix = Matrix.MatrixStretching(surroundingsMatrix,
                        gaussMapHeights[surroundingsIndex], gaussMapWidths[surroundingsIndex], stretchingFactor);

                featureMaps[i] = Matrix.MatrixSubstruction(centerMatrix,
                        stretchedSurroundingsMatrix, gaussMapHeights[centerIndex], gaussMapWidths[centerIndex]);
                //
                featureMaps[i] = Matrix.MatrixAbsolute(featureMaps[i], gaussMapHeights[centerIndex], 
                    gaussMapWidths[centerIndex]);
                //
                featureMapHeights[i] = gaussMapHeights[centerIndex];
                featureMapWidths[i] = gaussMapWidths[centerIndex];
            }
            return featureMaps;
        }

        public static double[][][] FeatureMapsBuilding2(double[][][] gaussPyramid, int[] gaussMapHeights, int[] gaussMapWidths,
         int[] centerIndices, int[] surroundingsIndices, int numOfFeatureMaps)
        {
            double[][][] featureMaps = new double[numOfFeatureMaps][][];

            for (int i = 0; i < numOfFeatureMaps; i++)
            {
                int centerIndex = centerIndices[i];
                int surroundingsIndex = surroundingsIndices[i];
                double[][] centerMatrix = gaussPyramid[centerIndex];
                double[][] surroundingsMatrix = gaussPyramid[surroundingsIndex];
                int stretchingFactor = (int)Math.Pow(2, surroundingsIndex - centerIndex);

                double[][] stretchedSurroundingsMatrix = Matrix.MatrixStretching(surroundingsMatrix,
                        gaussMapHeights[surroundingsIndex], gaussMapWidths[surroundingsIndex], stretchingFactor);

                featureMaps[i] = Matrix.MatrixSubstruction(centerMatrix,
                        stretchedSurroundingsMatrix, gaussMapHeights[centerIndex], gaussMapWidths[centerIndex]);
                //
                featureMaps[i] = Matrix.MatrixAbsolute(featureMaps[i], gaussMapHeights[centerIndex],
                    gaussMapWidths[centerIndex]);
                //
            }
            return featureMaps;
        }
        public static int[][][] GaussPyramidCreation(int matrixHeight, int matrixWidth,
           int numOfLevels, ref int[] heights, ref int[] widths, ref int[] expandedHeights,
           ref int[] expandedWidths, ref int[][][] expandedPyramid)
        {
            int[][][] pyramid = new int[numOfLevels][][];

            expandedPyramid = new int[numOfLevels][][];
            heights = new int[numOfLevels];
            widths = new int[numOfLevels];
            expandedHeights = new int[numOfLevels];
            expandedWidths = new int[numOfLevels];

            heights[0] = matrixHeight;
            widths[0] = matrixWidth;
            expandedHeights[0] = heights[0] + 2;
            expandedWidths[0] = widths[0] + 2;

            for (int i = 1; i < numOfLevels; i++)
            {
                int factor = (int)Math.Pow(2, i);
                heights[i] = matrixHeight / factor;
                widths[i] = matrixWidth / factor;
                expandedHeights[i] = heights[i] + 2;
                expandedWidths[i] = widths[i] + 2;
            }

            for (int i = 0; i < numOfLevels; i++)
            {
                pyramid[i] = new int[heights[i]][];
                expandedPyramid[i] = new int[expandedHeights[i]][];
                for (int j = 0; j < heights[i]; j++)
                    pyramid[i][j] = new int[widths[i]];
                for (int j = 0; j < expandedHeights[i]; j++)
                    expandedPyramid[i][j] = new int[expandedWidths[i]];
            }

            return pyramid;
        }

        public static void GaussPyramidFilling(int[][] inputMatrix, int numOfLevels, int[] heights, int[] widths,
           int[] expandedHeights, int[] expandedWidths, ref int[][][] gaussPyramid, ref int[][][] expandedPyramid)
        {
            int filterHeight = 4;
            int filterWidth = 4;
            int rightShift = 2;
            int downShift = 2;

            for (int i = 0; i < heights[0]; i++)
                for (int j = 0; j < widths[0]; j++)
                    gaussPyramid[0][i][j] = inputMatrix[i][j];

            for (int k = 1; k < numOfLevels; k++)       // ITERATIVE, NOT PARALLELIZABLE
            {
                for (int i = 0; i < heights[k - 1]; i++)
                    for (int j = 0; j < widths[k - 1]; j++)
                        expandedPyramid[k - 1][i + 1][j + 1] = gaussPyramid[k - 1][i][j];

                for (int j = 1; j < expandedWidths[k - 1] - 1; j++)
                {
                    expandedPyramid[k - 1][0][j] = expandedPyramid[k - 1][1][j];
                    expandedPyramid[k - 1][expandedHeights[k - 1] - 1][j] =
                        expandedPyramid[k - 1][expandedHeights[k - 1] - 2][j];
                }
                for (int i = 0; i < expandedHeights[k - 1]; i++)
                {
                    expandedPyramid[k - 1][i][0] = expandedPyramid[k - 1][i][1];
                    expandedPyramid[k - 1][i][expandedWidths[k - 1] - 1] =
                        expandedPyramid[k - 1][i][expandedWidths[k - 1] - 2];
                }
                for (int i = 0; i < heights[k]; i++)
                    for (int j = 0; j < widths[k]; j++)
                    {
                        double sum = 0;
                        for (int fi = 0; fi < filterHeight; fi++)
                            for (int fj = 0; fj < filterWidth; fj++)
                                sum += expandedPyramid[k - 1][downShift * i + fi][rightShift * j + fj];
                        sum = sum / 16;
                        gaussPyramid[k][i][j] = (int)Math.Round(sum);
                    }
            }
        }
        public static int[][][] GaussPyramidCreation(int[] heights, int[] widths, int numOfLevels)
        {
            int[][][] pyramid = new int[numOfLevels][][];
            for (int i = 0; i < numOfLevels; i++)
            {
                pyramid[i] = new int[heights[i]][];
                for (int j = 0; j < heights[i]; j++)
                    pyramid[i][j] = new int[widths[i]];
            }
            return pyramid;
        }
    }
}
